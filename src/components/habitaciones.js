import React from "react"
import { graphql } from "gatsby"
import Layout from "./layout"
import Image from "gatsby-image"
import styled from "@emotion/styled"
import {css} from '@emotion/react'

export const query = graphql`
query ($slug: String!) {
  allDatoCmsAbitacion(filter: {slug: {eq: $slug}}) {
    nodes {
      slug
      titulo
      contenido
      imagen {
        fluid {
          ...GatsbyDatoCmsFluid
        }
      }
    }
  }
}`


const ContentProduct = styled.div`
  max-width: 700px;
  margin: auto;
`


const HabitacionesTemplate = ({ data: { allDatoCmsAbitacion: { nodes } } }) => {

  const { titulo, contenido, imagen } = nodes[0]

  return (<Layout>
    <ContentProduct>
      <h1>{titulo}</h1>
      <p>{contenido}</p>
      <Image css={css`margin-bottom: 100px; margin-top: 40px`} fluid={imagen.fluid} alt={titulo} />
    </ContentProduct>
  </Layout>)
}
export default HabitacionesTemplate
