import React from 'react'
import {graphql, useStaticQuery} from "gatsby"
import Image from 'gatsby-image'

import styled from "@emotion/styled"
import {css} from '@emotion/react'


const TextNosotros =  styled.div`
  padding: 4rem;
  max-width: 1200px;
  width: 95%;
  margin: auto;
  @media(min-width: 768px){
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    column-gap: 10rem;
  }
  p{
    line-height: 3rem;
  }
`

const ContenidoNosotros = () => {
  const info = useStaticQuery(graphql`query{
  allDatoCmsPagina(filter: {slug: {eq: "nosotros"}}) {
    nodes {
      titulo
      contenido
      imagen {
        fluid {
          ...GatsbyDatoCmsFluid
        }
      }
    }
  }
}`);

  const {titulo, contenido, imagen} = info.allDatoCmsPagina.nodes[0]

  return(
    <>
      <h2 css={css`
              text-align: center; 
              font-size: 4rem;
              margin-top: 4rem;     
            `}>{titulo}</h2>
      <TextNosotros>
        <p>{contenido}</p>
        <Image fluid={imagen.fluid} alt={titulo} />
      </TextNosotros>
    </>
  )

}

export default  ContenidoNosotros
