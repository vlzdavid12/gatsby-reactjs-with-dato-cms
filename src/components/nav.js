import React from "react"
import {Link} from "gatsby"
import styled from "@emotion/styled"

const Nav = styled.nav`
  display: flex;
  justify-content:  center;
  padding-bottom:  3rem;
  @media(min-width: 760px){
    padding-bottom: 10px;
  }
`

const NavLink = styled(Link)`
    color: var(--white);
    font-size: 1.6rem;
    font-weight: 600;
    line-height: 1rem;
    font-family: "Roboto", sans-serif;
    text-decoration: none;
    margin: 10px;
    padding: 10px;
    &:hover{
      cursor: pointer;
      background: var(--red);
      color: var(--white);
      border-radius: 2px;
    }
    &.active{
      background: var(--black);
      color: var(--white)
    }
`

const Navegation = () => {
  return(<Nav>
    <NavLink
      to={'/'}
      activeClassName="active"
    >Inicio</NavLink>
    <NavLink
      activeClassName="active"
      to={'/nosotros'}
    >Nosotros</NavLink>
  </Nav>)
}
export default  Navegation;
