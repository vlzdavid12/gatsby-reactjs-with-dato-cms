import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import BackgroundImage from "gatsby-background-image"
import styled from "@emotion/styled"


const ImageBackground = styled(BackgroundImage)`
  height: 700px;
`

const TextImagen = styled.div`
  background-image: linear-gradient(to top, rgba(34, 49, 63, .8), rgba(34, 49, 63, .8));
  color: #000;
  height: 100%;
  display: flex;
  flex-direction: column;
  flex: 1;
  align-items: center;
  justify-content: center;

  h1 {
    color: var(--white);
    @media (min-width: 992px) {
      font-size: 5.8rem;
    }
  }

  p {
    color: var(--white);
    @media (min-width: 992px) {
      font-size: 2.6rem;
    }
  }
`


const ImagenHotel = () => {

  const { image } = useStaticQuery(graphql`
              query{
                image: file(relativePath: {eq: "8.jpg"}){
                sharp: childImageSharp{
                 fluid(maxWidth: 2000){
                   ...GatsbyImageSharpFluid
                 }
              }
            }
        }`)

  return (<ImageBackground tag="section" fluid={image.sharp.fluid} >
    <TextImagen>
      <h1>Bienvenido al Hotel Gatsby</h1>
      <p>Es el mejor Hotel para tus vacaciones</p>
    </TextImagen>
  </ImageBackground>)

}

export default ImagenHotel
