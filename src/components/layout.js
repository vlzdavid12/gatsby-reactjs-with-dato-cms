import React from "react"
import Header from "./header"
import Footer from "./footer"
import Helmet from "react-helmet"
import { Global, css } from "@emotion/react"
import UseSeo from "../hooks/use-seo"

const Layout = (props) => {
  const seo = UseSeo();
  const {title, description} = seo;
  return (
    <>
      <Global
        styles={
          css`
            :root {
              --red: #f30000;
              --green: #0b8f08;
              --blue: #0a4e98;
              --naranja: #d6441b;
              --white: #fff;
              --black: #0b2e66;
            }
            *,*:before, *:after{
              box-sizing: inherit;
            }
            html {
              font-size: 62.5%;
              overflow-x: hidden;
              height: 100%;
              box-sizing: border-box;
            }

            body {
              font-size: 16px;
              font-size: 1.6rem;
              line-height: 1.5;
              min-height: 100%;
            }

            h1, h2, h3 {
              margin: 0;
              line-height: 1.5;
            }

            h1, h2, h3 {
              font-family: Antonio, sans-serif;
            }

            p {
              font-family: Roboto, sans-serif;
            }
            
            a{
              text-decoration: none;            
            }

            ul {
              list-style: none;
              padding: 0px;
              margin: 0px;
            }

          `
        }
      />
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css"
              integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w=="
              crossOrigin="anonymous" />
        <link href="https://fonts.googleapis.com/css2?family=Antonio&family=Roboto:wght@300&display=swap"
              rel="stylesheet" />
      </Helmet>
      <Header />
      {props.children}
      <Footer title={title} />
    </>
  )
}
export default Layout
