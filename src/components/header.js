import React from 'react'
import {css} from '@emotion/react'
import Nav from "./nav"
import { Link } from "gatsby"
import styled from "@emotion/styled"

const Logo =  styled(Link)`
  color:#fff;
  text-align: center;
  font-size: 3rem;
  font-family: Antonio, sans-serif;
`


const Header = () =>{
  return(<header
  css={
    css`
        background-color: #092044; 
        padding: 1rem;   
`
  }
  >
    <div
    css={
      css`
      max-width: 1200px;  
      margin:  0 auto;
      @media (min-width: 768px){
          display: flex;
          align-items: center;
          justify-content: space-between;
      } 
      `
    }
    >
      <Logo to={'/'}>Hotel Gatsby</Logo>
      <Nav/>
    </div>
  </header>)
}
export default Header
