import React from 'react';
import styled from "@emotion/styled"
import { css } from "@emotion/react"
import Nav from "./nav"
import { Link } from "gatsby"


const Logo =  styled(Link)`
  color:#fff;
  text-align: center;
  font-size: 3rem;
  font-family: Antonio, sans-serif;
`

const Footer = ({title}) =>{

  const date = new Date().getFullYear();

  return(
    <>
    <footer
    css={
      css`
        background-color: #092044; 
        padding: 1rem;   
`
    }
  >
    <div
      css={
        css`
      max-width: 1200px;  
      margin:  0 auto;
      @media (min-width: 768px){
          display: flex;
          align-items: center;
          justify-content: space-between;
      } 
      `
      }
    >
      <Nav/>
      <Logo to={'/'}>Hotel Gatsby</Logo>

    </div>
  </footer>
      <div css={css`text-align: center; 
            background: #01112b;
            color: #fff;
            font-family: Roboto, sans-serif;
            padding: 1rem;`}>{title}, Todos los derechos reservados {date} &copy; </div>
  </>

  )
}
export default  Footer
