import {graphql, useStaticQuery} from "gatsby"

const useHabitaciones = () => {

  const infoHabitacion =  useStaticQuery(graphql`
  query {
  allDatoCmsAbitacion {
    nodes {
      id
      titulo
      contenido
      slug
      imagen {
        fluid(maxWidth: 1200) {
          ...GatsbyDatoCmsFluid
        }
      }
    }
  }
}`);


  return infoHabitacion.allDatoCmsAbitacion.nodes.map(habitacion=>({
    id: habitacion.id,
    title: habitacion.titulo,
    content: habitacion.contenido,
    slug: habitacion.slug,
    img: habitacion.imagen

  }))

}

export default useHabitaciones
