import {graphql, useStaticQuery} from "gatsby"

const UseSeo = () =>{

  const data = useStaticQuery(graphql`query{
  datoCmsSite {
    globalSeo {
      siteName
      titleSuffix
      fallbackSeo {
        title
        description
        image {
          url
        }
      }
    }
  }
}`)

  return data.datoCmsSite.globalSeo.fallbackSeo;


}

export default  UseSeo;
