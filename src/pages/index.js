import * as React from "react"
import Layout from "../components/layout"
import ImagenHotel from "../components/imagenHotel"
import ContenidoInicio from "../components/contenidoinicio"
import useHabitaciones from "../hooks/use-habitaciones"
import { css } from "@emotion/react"
import styled from "@emotion/styled"
import Image from "gatsby-image"
import {Link} from "gatsby"

const List = styled.li`
  width: 90%;
  margin: 10px auto;
  padding: 0.50rem;
  border: 1px dashed #d9d9d9;
  @media (min-width: 790px) {
    width: 33%;
    margin: 10px 5px;
  }
`

const ContentGeneral = styled.ul`
  max-width: 1000px;
  display: block;
  margin: auto;
  margin-bottom: 4rem;
  @media(min-width: 790px){
    display: flex;
  }
`

const Button = styled(Link)`
  background: var(--red);
  color: var(--white);
  padding: 10px;
  border:0px;
  width: 100%;
  text-align: center;
  font-family: Roboto, sans-serif;
  display: block;
  
  &:hover{
    cursor: pointer;
    background: var(--black);
  }

`

const IndexPage = () => {

  const habitaciones = useHabitaciones()

  return (
    <Layout>
      <ImagenHotel />
      <ContenidoInicio />

      <h2 css={
        css`
          text-align: center;
          margin-top: 5rem;
          font-size: 3rem;
          margin-bottom: 5rem;
        `
      }>Nuestras Habitaciones</h2>
      <ContentGeneral>
        {
          habitaciones.map(list => (<List>
            <div>
              <Image css={css`height: 220px;
                width: auto; border-radius: 4px`} fluid={list.img.fluid} alt={list.title} />
            </div>
            <div css={css`margin-top: 2rem;
              text-align: center`}>
              <h3>{list.title}</h3>
            </div>
            <div css={css`margin-top: 2rem;
              text-align: justify`}>
              {list.content.length > 120 ? <p>{list.content.substr(0, 110) + " [...]"}</p> : <p>{list.content}</p>}
            </div>
            <Button to={list.slug} >Ver mas</Button>
          </List>))
        }
      </ContentGeneral>

    </Layout>
  )
}

export default IndexPage
