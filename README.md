<!-- AUTO-GENERATED-CONTENT:START (STARTER) -->

![alt gastby-react](https://gitlab.com/vlzdavid12/gatsby-reactjs-with-dato-cms/-/raw/master/src/images/screenshop.png)

<h1 align="center">
   <a href="https://www.gatsbyjs.com">
    <img alt="Gatsby" src="https://www.gatsbyjs.com/Gatsby-Monogram.svg" width="60" />
  </a> Gatsby's default starter with Dato Cms
</h1>


## 🚀 Quick start

1.  **Create a Gatsby site.**

    Use the [Gatsby CLI](https://www.npmjs.com/package/gatsby-cli) to create a new site, specifying the default starter.

2.  **Start developing.**

    Navigate into your new site’s directory and start it up.

    ```shell
    cd my-default-starter/
    gatsby develop
    ```

3.  **Open the source code and start editing!**

    Your site is now running at `http://localhost:8000`!

    _Note: You'll also see a second link: _`http://localhost:8000/___graphql`_. This is a tool you can use to experiment with querying your data. Learn more about using this tool in the [Gatsby tutorial](https://www.gatsbyjs.com/tutorial/part-five/#introducing-graphiql)._

    Open the `my-default-starter` directory in your code editor of choice and edit `src/pages/index.js`. Save your changes and the browser will update in real time!

2.  **Start project production.**
   
    In the /public/ folder is to upload has production.
   
     ```shell
     cd my-default-starter/
     gatsby build
     ```
     
## DEMO 😍
[Gatsby Standar Web](https://laughing-engelbart-a0550a.netlify.app/)
