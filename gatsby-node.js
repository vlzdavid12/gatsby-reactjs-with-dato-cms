exports.createPages = async({actions, graphql, reporter})=>{
    const result = await graphql(`
    query{
  allDatoCmsAbitacion {
    nodes {
      slug 
    }
  }
}
    `);

    //console.log(result.data.allDatoCmsAbitacion.nodes)

    if(result.errors){
        reporter.panic('No Hubo Resultados', result.errors)
    }

    const slugHabitacion = result.data.allDatoCmsAbitacion.nodes;

  slugHabitacion.forEach(page => {
        actions.createPage({
                path: page.slug,
                component: require.resolve('./src/components/habitaciones.js'),
                context: {
                    slug: page.slug
                }
          })
    })
}


